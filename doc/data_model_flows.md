# Data Change Flows

`TODO: update with the simplified model. this will be pulled out into a writeup of additional management features.`

This describes the flows of data and changes on a higher level to keep a reference which can then be cross-checked with the code in case i forget things.

It may seem trivial when or after looking at the diagrams, but it still helps a lot as those flows don't have to be kept in mind constantly while programming.

## Common Concepts

Members are Users as defined in the database schema.

### Fetch a Model

```mermaid
graph LR
    A[Model]-->|Is public|B[Return data]
    A-->|Is not public|C[Has Members]
    C-->|yes|D[Current User]
    D-->|Is a member|E[Return data]
    D-->|Is an admin|F[Log access]
    F-->E
    D-->|Not a member|G[Access denied]

    C-->|no|H[Current User]
    H-->|Is not Owner|I[Access denied]
    H-->|Is Owner|J[Return data]
    H-->|Is an admin|K[Log access]
    K-->J
```

### Update or Delete a Model

```mermaid
graph LR
    A[Model]-->|Has members|B[Current User]
    B-->|Not a member|C[Access denied]
    B-->|Is a member|D[Return data]
    B-->|Is an admin|E[Log access]    
    E-->D

    A-->|Has no members|F[Current User]
    F-->|Is not Owner|G[Access denied]
    F-->|Is Owner|H[Return data]
    F-->|Is an admin|I[Log access]
    I-->H
    
```

## Bench

### Make a Bench public

```mermaid
graph LR
    A[Bench]-->B[Projects]
    B-->|Is Project public?|B
    B-->|All refs public|C[Return data]
    B-->|Contains private projects|D[Show list of private refs]
```

## Project

Project flows are a bit less complicated than Benches, but especially great care needs to be taken with regards to Categories.
