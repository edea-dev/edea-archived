# Writeups

A list of topics i want to tackle in future EDeA blog posts:

- Authentication & Authorization
  - this has been half-written, needs finishing.
- Using Row Level Security to implement complex User constraints
  - Need to pull out the SQL code i wrote for EDeA management features
  - Transform it into nice example code plus example data for people to play around with
- Your first KiCad Project with EDeA
  - An article plus a Video showing how to create something with EDeA with available modules
