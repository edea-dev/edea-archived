# User Journeys

Here's a high-level overview as to how users tend to use EDeA. It (should) cover all the common workflows which are expected.

The satisfaction level should later be adjusted to actual user experiences from feedback. This way we can then make the process as easy as possible for others.

## Creating a new Bench

This should be as straight-forward as possible, as it will be the most common workflow on the whole site.

```mermaid
journey
    title New Bench
    section First steps
      Login: 5: Me
      Click New Bench: 5: Me
      Set name: 4: Me
    section Add Project
      Type into search bar: 5: Me, Repeated
      View results: 3: Me
      Filter results: 4: Me, Repeated
      View a Project: 5: Me
      Click Add to Bench: 5: Me
    section Build Bench
     Click Build Bench: 5: Me
     Download File: 5: Me
```

## Creating a new Project

When creating a new project, by default it will get a random uuid and will only be accessible by `/project/{uuid}`. The user can then later on choose a name tuple if wanted, e.g. `/{username}/{project_short}` or `/{group_name}/{project_short}`.

```mermaid
journey
    title New Project
    section Mandatory steps
      Login: 5: Me
      Click New Project: 5: Me
      Set name: 4: Me
      Set Repository: 4: Me
      Click Add Project: 5: Me
      View newly added Project: 5: Me
    section Additional steps
      Set a short name or Organization: 5: Me
      Add Contributors: 3: Me
```

## Login

The Login process will vary quite a bit depending on which provider is used to handle the authentication, so this only covers the part where the user currently is on the EDeA portal.

```mermaid
journey
    title Login
    section Mandatory steps
      Click Login: 5: Me
      Click Provider Icon or enter Username/Password: 5: Me
      Redirect to Auth provider: 3: Me
      Return from Auth provider: 5: Me
      Redirect to original target: 5: Me
```

## Logout

Click a button and that's it :3

## Register

TODO

## Delete Account

TODO

## Export all data

TODO

## Add Member to Project

TODO

## Add Member to Bench

TODO

## Remove Member from Project

TODO

## Remove Member from Bench

TODO

## Modify Member Status in a Project

TODO

## Modify Member Status in a Bench

TODO

## Modify Member Status in an Organization

TODO
