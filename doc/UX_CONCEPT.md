# UX concept

## Concepts
 - Project: A 1:1 association to a repository containing a circuit with optional documentation and tests, firmware / drivers, etc. Can be either a Module or an Assembly.
 - Module: A circuit schematic and layout with clearly defined in/out ports (KiCad hierarchical labels).
 - Assembly: A Project which consists of one or more Modules, assembled into a valid KiCad project. Possibility to put every Module of an Assembly onto the Bench again.
 - Bench: Shopping-cart-like temporary storage of Modules. A Bench gets compiled into Assemblies when the User considers the selection done. (Does it make sense to support having multiple active Benches simultaneously?)

## Notes
 - Without login: searching, viewing, and downloading Projects (Assemblies, Modules)
 - With login:
  - New Module
  - Edit Modules (which are associated with the user, or user has write rights)
  - Put Module(s) on the Bench
  - Assemble Modules stored on the Bench -> Create Project
  - Create new Project (without being generated from Module(s))
  - Update Projects
  - Comment on any Project through 3rd party issue tracker

## Workflow

### For a New Circuit
 - Browse through Modules
 - Repeat:
   - Add to Bench
   - Remove from Bench
 - Finally: Assemble
 - Any time: empty Bench

### Adding a new Project
 - Enter name and git repo url
  - Display if the repo has already been referenced in another project
 - Fetch repository from remote
 - Add module to database

## Views

### Landing Page / during early EDeA development
 - About EDeAhub, elevator pitch?
 - Getting Started
 - Link to sources, bug tracker

### Landing Page / with a mature EDeA
 - Getting Started
 - Highlighted Modules, Projects

### Project Page - Assembly
 - Readme / About This Project
 - Visual diff between generated Assembly and current state

### Project Page - Module
 - Readme / About This Module
 - Update <- shouldn't we do this from "cron"?
  - WebHooks to update repos (GitLab, GitHub, Gitea)
  - Plain old job queue and git fetch latest revision for those that don't
 - Link to issue tracker (if exists) and project page on hosting service
 - Comment section? Maybe use a dedicated git* issue? Or future Fedi integration?
 - Fork tracking if from external site
  - Is it a fork? Link to module on EDeA if it is available
 - List of Projects using this Module

#### Info Bar
A bar like GitLab, Gitea have with:
 - Revisions
 - Branches
 - Open Issues?
 - Stars
 - License

#### Project Documentation Page
 - Display markdown files
  - Measurement data feature (mature product)


#### Layout / Schematic History
 - Render Schematics and Layout as SVG
  - Visual Diff with existing tools


### Showcase Page

Curated list (-> admin interface) of projects which display best practices

### Explore Page

List of projects?


## Important Features
 - Replace Module by an equivalent one seamlessly. Required in case the original gets deleted or abandoned.
 - Change ownership of project on EDeA
 - Display license of Module. 
  - License compatibility check (in the mature project)

### Lower Priority
 - Data export (give me all the data so i don't have to scrape it)
 - User Data export: export all the data of a user in a nice format (GDPR data portability)
