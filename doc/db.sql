DROP TABLE IF EXISTS "benches";
DROP TABLE IF EXISTS "profiles";
DROP TABLE IF EXISTS "projects";
DROP TABLE IF EXISTS "repositories";
DROP TABLE IF EXISTS "users";
DROP TABLE IF EXISTS "bench_projects";
DROP TABLE IF EXISTS "bench_members";
DROP TABLE IF EXISTS "project_members";


CREATE TABLE "benches" (
    "id" uuid DEFAULT gen_random_uuid(),
    "active" boolean,
    "public" boolean,
    "created" timestamptz DEFAULT now(),
    "name" text,
    "description" text,
    PRIMARY KEY ("id")
);

CREATE TABLE "profiles" (
    "id" uuid DEFAULT gen_random_uuid(),
    "display_name" text,
    "location" text,
    "biography" text,
    "avatar" text,
    PRIMARY KEY ("id")
);

CREATE TABLE "projects" (
    "id" uuid DEFAULT gen_random_uuid(),
    "repo_url" text,
    "name" text,
    "description" text,
    "public" boolean,
    PRIMARY KEY ("id")
);

CREATE TABLE "repositories" (
    "id" uuid DEFAULT gen_random_uuid(),
    "url" text,
    "type" text,
    "location" text,
    "updated" timestamptz,
    "created" timestamptz,
    PRIMARY KEY ("id")
);

CREATE TABLE "users" (
    "id" uuid DEFAULT gen_random_uuid(),
    "auth_uuid" text NOT NULL UNIQUE,
    "handle" text UNIQUE,
    "created" timestamptz DEFAULT now(),
    "is_admin" boolean DEFAULT false,
    "profile_id" uuid NOT NULL,
    PRIMARY KEY ("id"),
    UNIQUE ("auth_uuid", "handle")
);

CREATE TABLE "bench_projects" (
    "id" uuid DEFAULT gen_random_uuid(),
    "name" text,
    "description" text,
    "conf" jsonb,
    "project_id" uuid NOT NULL,
    "bench_id" uuid NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE "bench_members" (
    "user_id" uuid,
    "bench_id" uuid,
    "is_admin" boolean,
    PRIMARY KEY ("user_id", "bench_id")
);

CREATE TABLE "project_members" (
    "user_id" uuid,
    "project_id" uuid,
    "is_admin" boolean,
    PRIMARY KEY ("user_id", "project_id")
);

--
-- hand crafted dummy data from here on out
--
;

-- add some profiles first
INSERT INTO profiles VALUES 
    ('8d3a71ea-86e7-40fd-8084-b6e6b7148466', 'Elen', '', '', ''),
    ('a09b073c-b4a3-410c-8608-3003ffa8a123', 'lownoise', '', '', ''),
    ('d3248989-9096-449d-9a0f-8247d624208e', 'Zoé', '', '', ''),
    ('0ca9c5d1-5cec-448d-8d60-fc5c1efab4d0', 'Alice', '', '', ''),
    ('8e1a1cf2-34c2-4e30-be96-3a87265ad941', 'Bob', '', '', '');

-- now for some users
INSERT INTO users VALUES 
    ('ee65d685-6675-46a8-bc7f-eacf0c27a9e1', '080012f1-32f9-4401-9453-414ba02786bd', 'elen', DEFAULT, true, '8d3a71ea-86e7-40fd-8084-b6e6b7148466'),
    ('b37edc39-6173-44f5-93bc-e74e3e059d5e', '6c27090b-2106-4e16-9803-5053c49f77c7', 'lownoise', DEFAULT, false, 'a09b073c-b4a3-410c-8608-3003ffa8a123'),
    ('c94f3775-2124-470a-9804-7eeec93d63a4', '5a27b227-4935-4c5e-a1b1-c437b66b68a9', 'zoe', DEFAULT, true, 'd3248989-9096-449d-9a0f-8247d624208e'),
    ('f573868d-d999-4b0a-9702-749c86cf6738', '26887b64-1f04-485a-b404-434d2cbf01e7', 'alice', DEFAULT, false, '0ca9c5d1-5cec-448d-8d60-fc5c1efab4d0'),
    ('41b0a08e-984c-413e-ae21-a188d3168c69', '50c3d896-454f-42a5-8380-aaca04da416b', 'bob', DEFAULT, false, '8e1a1cf2-34c2-4e30-be96-3a87265ad941');

-- Projects
INSERT INTO projects VALUES
    ('22815d7d-79a5-4ed5-9e61-637fbdcfc8d8', 'https://github.com/some/thing', 'A project', 'Nothing to see here, literally', true),
    ('5386182d-2341-4f1d-8f9a-9b9644f5ba39', 'https://github.com/another/thing', 'A cool module', '', true),
    ('6606c48e-9bec-4eac-be30-4e632c8d08e0', 'https://github.com/fullyautomated/edea_sample_1', 'Sample Project', '', true),
    ('cf4683a6-7f94-41c2-b5fc-f8050197a28e', 'https://github.com/fullyautomated/private_project', 'A private project', 'This will be hidden from users who are not members.', false),
    ('fabca210-63f6-4ec3-9e60-21d01ad3b1e4', 'https://github.com/alice/private_project', 'Another private project', 'This will be hidden from users who are not members.', false);


INSERT INTO project_members VALUES
    ('ee65d685-6675-46a8-bc7f-eacf0c27a9e1', 'cf4683a6-7f94-41c2-b5fc-f8050197a28e', true), -- elen, admin of private_project
    ('c94f3775-2124-470a-9804-7eeec93d63a4', 'cf4683a6-7f94-41c2-b5fc-f8050197a28e', true), -- zoe, admin of private_project
    ('b37edc39-6173-44f5-93bc-e74e3e059d5e', '6606c48e-9bec-4eac-be30-4e632c8d08e0', true), -- lownoise, admin of 'Sample Project'
    ('b37edc39-6173-44f5-93bc-e74e3e059d5e', '5386182d-2341-4f1d-8f9a-9b9644f5ba39', true), -- lownoise, admin of 'A cool module'
    ('b37edc39-6173-44f5-93bc-e74e3e059d5e', 'cf4683a6-7f94-41c2-b5fc-f8050197a28e', true), -- lownoise, member of private_project
    ('f573868d-d999-4b0a-9702-749c86cf6738', '22815d7d-79a5-4ed5-9e61-637fbdcfc8d8', true), -- alice, admin of 'A project'
    ('41b0a08e-984c-413e-ae21-a188d3168c69', '22815d7d-79a5-4ed5-9e61-637fbdcfc8d8', false), -- bob, member of 'A project'
    ('f573868d-d999-4b0a-9702-749c86cf6738', 'fabca210-63f6-4ec3-9e60-21d01ad3b1e4', true); -- alice, admin of 'Another private project'





-- select all projects visible to a user by their auth_uuid
explain select p.*
from users u
join project_members pm on u.id = pm.user_id
join projects p on pm.project_id = p.id
where p.public
    or (u.auth_uuid = '26887b64-1f04-485a-b404-434d2cbf01e7'
        and (u.is_admin = true
             or (pm.project_id = p.id
                 and pm.user_id = u.id
                 and pm.is_admin)));


-- select if a user has the permissions to make a change to a project
select u.is_admin as "admin",
       pm.is_admin as "project_admin",
       u.handle
from users u
join project_members pm on u.id = pm.user_id
where u.auth_uuid = '26887b64-1f04-485a-b404-434d2cbf01e7'
    and (u.is_admin = true
         or (pm.project_id = '22815d7d-79a5-4ed5-9e61-637fbdcfc8d8'
             and pm.user_id = u.id
             and pm.is_admin));


-- returns true if the current session user has permissions to change the project
CREATE OR REPLACE FUNCTION is_project_admin (_project_id UUID) RETURNS BOOLEAN AS $$
DECLARE
    v_id        TEXT;
    v_admin     BOOLEAN;
    v_prj_admin BOOLEAN;
BEGIN

    -- read the value from session variable
    v_id := current_setting('edea.session_id');

    SELECT u.is_admin, pm.is_admin INTO v_admin, v_prj_admin
        FROM users u
        JOIN project_members pm ON u.id = pm.user_id
        WHERE u.auth_uuid = v_id
            AND (u.is_admin = true
                OR (pm.project_id = _project_id
                    AND pm.user_id = u.id
                    AND pm.is_admin));

    -- given user does not have permissions
    IF NOT FOUND THEN
        RAISE EXCEPTION 'forbidden';
    END IF;

    -- user may perform the changes
    RETURN TRUE;

END;
$$ LANGUAGE plpgsql SECURITY DEFINER STABLE;

GRANT ALL ON FUNCTION is_project_admin() TO PUBLIC;


-- policy which allows only (project) admins to change the projects table
CREATE POLICY project_select_policy ON projects FOR SELECT USING (TRUE);
CREATE POLICY project_insert_policy ON projects FOR INSERT USING (TRUE);
CREATE POLICY project_update_policy ON projects FOR UPDATE WITH CHECK (is_project_admin(id));
CREATE POLICY project_delete_policy ON projects FOR DELETE WITH CHECK (is_project_admin(id));

-- test our permission system
BEGIN;
SET LOCAL edea.session_id = '6c27090b-2106-4e16-9803-5053c49f77c7';
SELECT is_project_admin('fabca210-63f6-4ec3-9e60-21d01ad3b1e4');
ROLLBACK;
