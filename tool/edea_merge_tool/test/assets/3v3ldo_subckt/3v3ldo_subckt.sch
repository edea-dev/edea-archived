EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "A 3.3-v LDO as a demo"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6600 3750 7050 3750
Wire Wire Line
	5250 3700 5000 3700
Wire Wire Line
	5250 4300 5000 4300
Text HLabel 4750 4300 0    50   UnSpc ~ 0
EGND
Text HLabel 4750 3700 0    50   Input ~ 0
EVIN
Text HLabel 7050 3750 2    50   Output ~ 0
EVOUT
$Sheet
S 5250 3550 1350 900 
U 5E2671D2
F0 "Test Sheet" 50
F1 "embedded_sheet.sch" 50
F2 "GND" U L 5250 4300 50 
F3 "VIN" I L 5250 3700 50 
F4 "VOUT" O R 6600 3750 50 
$EndSheet
$Comp
L Device:C_Small C?
U 1 1 5E26D017
P 5000 4000
AR Path="/5E2671D2/5E26D017" Ref="C?"  Part="1" 
AR Path="/5E26D017" Ref="C101"  Part="1" 
F 0 "C101" H 4908 4091 50  0000 R CNN
F 1 "1uF" H 4908 4000 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5000 4000 50  0001 C CNN
F 3 "~" H 5000 4000 50  0001 C CNN
F 4 "SomePartNumber" H 4908 3909 50  0000 R CNN "PartNo"
	1    5000 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4100 5000 4300
Connection ~ 5000 4300
Wire Wire Line
	5000 4300 4750 4300
Wire Wire Line
	5000 3900 5000 3700
Connection ~ 5000 3700
Wire Wire Line
	5000 3700 4750 3700
$EndSCHEMATC
