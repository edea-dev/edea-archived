"""
``kicad_pcb`` parser using `sexp_parser.SexpParser`

The parser `KicadPCB` demostrates the usage of a more gernal S-expression
parser of class `sexp_parser.SexpParser`. Check out the source to see how easy
it is to implement a parser in an almost declarative way.

Copyright 2016, Zheng, Lei

Originally part of https://github.com/realthunder/kicad_parser
Changes: Copyright (c) 2021 Fully Automated OÜ

SPDX-License-Identifier: MIT
"""

from typing import OrderedDict
import sexpparser as sp
from boundingbox import BoundingBox
import math
import numpy as np
import empty_pcb

""" notes
pcb.module[i].pad[j][0] -> pad number
                    [1] -> pad type (smd or thru_hole)
                    [2] -> pad shape: {circle, custom, oval, rect, roundrect} (is there more?)
"""


class PCBlayerAbstraction:
    """
    Helper class because the way layers are stored is unwieldly for
    any kind of modifications with the barefoot library.
    TODO consider integrating this into the KicadPCB parser?
    """

    def __init__(self, layers: sp.SexpParser):
        self.data = {}
        for layer in layers:
            id = int(layer)
            self.data[id] = [layer, *(layers[layer])]

    def addLayer(self, id, layerarray):
        self.data[int(id)] = [str(id), *layerarray]

    def merge(self, otherlayers):
        selfset = set(self.data)
        otherset = set(otherlayers.data)
        mergedset = selfset.union(otherset)
        data = {}
        added = 0
        for id in mergedset:
            if id in selfset:
                data[id] = self.data[id]
            else:
                data[id] = otherlayers.data[id]
                added += 1
        self.data = data
        return added

    def setAllVisible(self):
        for elem in self.data:
            if len(self.data[elem]) > 2:
                self.data[elem][2] = ""

    def getSexpObject(self):
        sexprstr = "(layers\n"
        for label in self.data:
            layer = self.data[label]
            layerstr = " ".join(layer)
            sexprstr += "({})\n".format(layerstr)
        sexprstr += "\n)"
        return sp.SexpParser(sp.parseSexp(sexprstr))


class Movable(sp.SexpParser):
    """
    This class should be the superclass to all objects,
    which are referenced by global coordinates.
    """

    __slots__ = ()

    def _movexy(self, x, y):
        interests = ["at", "xy", "start", "end", "center"]
        coords = x, y
        for specifier in interests:
            if hasattr(self, specifier):
                for i in range(2):
                    self[specifier][i] = round(self[specifier][i] + coords[i], 2)


class KicadPCB_gr_text(sp.SexpParser):
    __slots__ = ()
    _default_bools = "hide"


class KicadPCB_drill(sp.SexpParser):
    __slots__ = ()
    _default_bools = "oval"


class KicadPCB_pad(sp.SexpParser):
    __slots__ = ()
    _parse1_drill = KicadPCB_drill

    def _parse1_layers(self, data):
        if not isinstance(data, list) or len(data) < 3:
            raise ValueError("expects list of more than 2 element")
        return sp.Sexp(data[1], data[2:], data[0])

    @property
    def _corners(self):
        """
        Returns a numpy array containing every corner [x,y]
        """
        points = []

        if len(self.at) > 2:
            angle = self.at[2] / math.tau
        else:
            angle = 0
        origin = self.at[0:2]

        if (
            self[2] == "rect"
            or self[2] == "roundrect"
            or self[2] == "oval"
            or self[2] == "custom"
        ):
            # TODO optimize this, this is called quite often

            points = np.array(
                [[1, 1], [1, -1], [-1, 1], [-1, -1]], dtype=np.float64)
            if self[2] == "oval":
                points = np.array(
                    [[1, 0], [-1, 0], [0, 1], [0, -1]], dtype=np.float64)
            w = self.size[0] / 2
            h = self.size[1] / 2
            c = math.cos(angle)
            s = math.sin(angle)
            for i in range(len(points)):
                points[i] = origin + points[i] * [(w * c + h * s), (h * c + w * s)]

        elif self[2] == "circle":
            points = np.array([[1, 0], [-1, 0], [0, 1], [0, -1]], dtype=np.float64)
            r = self.size[0] / 2
            for i in range(len(points)):
                points[i] = origin + points[i] * [r, r]

        else:
            raise NotImplementedError(
                "pad shape {} is not implemented".format(self[2]))

        return points


class KicadPCB_module(sp.SexpParser):
    """
    To get the bounding box, we calculate a bounding box for each pad,
    then iterate over all the pads while enlarging the module's bounding box
    At the end, we got a rectangle that surely covers every copper pad
    """

    __slots__ = ()
    _default_bools = "locked"
    _parse_fp_text = KicadPCB_gr_text
    _parse_pad = KicadPCB_pad

    @property
    def _boundingBox(self) -> BoundingBox:
        if len(self.pad) == 0:
            bb = BoundingBox([])
        else:
            bb = BoundingBox(self.pad[0]._corners)
        if len(self.pad) > 1:
            for i in range(1, len(self.pad)):
                bb.envelop(self.pad[i]._corners)

        if len(self.at) > 2:
            bb.rotate(self.at[2])
        bb.translate(self.at[0:2])
        return bb


class KicadPCB(sp.SexpParser):

    # To make sure the following key exists, and is of type SexpList
    _defaults = (
        "net",
        ("net_class", "add_net"),
        "dimension",
        "gr_text",
        "gr_line",
        "gr_circle",
        "gr_arc",
        "gr_curve",
        "gr_poly",
        "segment",
        "arc",
        "via",
        ("module", "fp_text", "fp_line", "fp_circle",
         "fp_arc", "fp_poly", "pad", "model"),
        ("zone", "polygon", "filled_polygon"),
    )

    _parse_module = KicadPCB_module
    _parse_gr_text = KicadPCB_gr_text

    def export(self, out, indent="  "):
        """
        Writes this pcb object into the file specified under parameter out
        """
        sp.exportSexp(self, out, "", indent)

    def getError(self):
        return sp.getSexpError(self)

    def getBoundingBox(self) -> BoundingBox:
        """
        Calculates and returns the bounding box of this PCB.
        Results are not cached.
        """
        if len(self.module) == 0:
            bb = BoundingBox([])
        else:
            bb = self.module[0]._boundingBox
        if len(self.module) > 1:
            for i in range(1, len(self.module)):
                bb.envelop(self.module[i]._boundingBox.corners)
        return bb

    def movexy(self, x, y):
        """
        Move everything by x,y on this PCB
        """
        interests = ["at", "xy", "start", "end", "center"]
        coords = x, y
        # Scalpel method
        to_be_moved = [
            "module",
            "gr_text",
            "gr_poly",
            "gr_line",
            "gr_arc",
            "via",
            "segment",
            "dimension",
            "gr_circle",
            "gr_curve",
            "arc",
        ]
        components_processed, coordiantes_processed = 0, 0
        for label in to_be_moved:
            container = self[label]
            if len(container) == 0:
                continue
            container_instance = 0
            for instance in container:
                for specifier in interests:
                    if hasattr(instance, specifier):
                        location = instance[specifier]
                        for i in range(2):
                            location[i] += coords[i]
                        """
                        print(
                            "[{:04d}] processed {}[{}], g. coordinate pair nr. {:05d}".format(
                                components_processed,
                                label,
                                container_instance,
                                coordiantes_processed,
                            ),
                            flush=True,
                        )
                        print(
                            "\t{}\tnew location ({}): {}".format(
                                label, specifier, location
                            ),
                            flush=True,
                        )
                        """
                        coordiantes_processed += 1
                components_processed += 1
                container_instance += 1

        # TODO: copy and adjust the zone logic for gr_poly too, as it's currently not handled

        """
        At this point I'm fairly convinced that the KiCad file formats were
        invented by programmers in order to sell more programs.
        """
        zones_processed = 0
        if len(self.zone) > 0:
            for z in self.zone:
                if len(z.polygon) == 0:
                    print(
                        "zone[{}] has empty polygon".format(zones_processed), flush=True
                    )
                    assert len(z.filled_polygon == 0)
                    continue
                polygons_processed = 0
                for poly in z.polygon:
                    if len(poly.pts) == 0:
                        continue
                    assert len(poly.pts) == 1
                    i = 0
                    arr = np.full((len(poly.pts.xy), 2), coords, dtype=np.float64)
                    for location in poly.pts.xy:
                        arr[i] += np.asarray(location)
                        i += 1
                        coordiantes_processed += 1
                    del poly.pts
                    sexprstr = "(pts "
                    for point in arr:
                        sexprstr += "(xy {:.5f} {:.5f}) ".format(*point)
                    sexo = sp.SexpParser(sp.parseSexp(sexprstr + ")"))
                    poly.pts = sexo
                    """
                    print(
                        "[{:04d}] processed zone[{}] polygon, g. coordinate pair nr. {:05d}".format(
                            components_processed, zones_processed, coordiantes_processed
                        ),
                        flush=True,
                    )
                    """
                    polygons_processed += 1

                if len(z.filled_polygon) == 0:
                    print("zone[{}] has no filled polygon".format(zones_processed))
                    continue
                fpolygons_processed = 0
                for fpoly in z.filled_polygon:
                    if len(fpoly.pts) == 0:
                        print(
                            "zone[{}] has filled polygon but no pts".format(
                                zones_processed
                            )
                        )
                        continue
                    assert len(fpoly.pts) == 1
                    i = 0
                    arr = np.full((len(fpoly.pts.xy), 2), coords, dtype=np.float64)
                    for location in fpoly.pts.xy:
                        arr[i] += np.asarray(location)
                        i += 1
                        coordiantes_processed += 1
                    del fpoly.pts
                    sexprstr = "(pts "
                    for point in arr:
                        sexprstr += "(xy {:.5f} {:.5f}) ".format(*point)
                    sexo = sp.SexpParser(sp.parseSexp(sexprstr + ")"))
                    fpoly.pts = sexo
                    """
                    print(
                        "[{:04d}] processed zone[{}] filled_polygon, g. coordinate pair nr. {:05d}".format(
                            components_processed, zones_processed, coordiantes_processed
                        ),
                        flush=True,
                    )
                    """
                    fpolygons_processed += 1
                components_processed += 1
                zones_processed += 1

    @staticmethod
    def formatToString(number: float) -> str:
        return "{:.2f}".format(number)

    @staticmethod
    def coordsToSexp(identifier: str, coords) -> sp.Sexp:
        """
        Returns a Sexp with identifier as keyword and two coordinates
        """
        x, y = coords
        xstr = KicadPCB.formatToString(x)
        ystr = KicadPCB.formatToString(y)
        se = sp.Sexp(identifier, [xstr, ystr])
        return se

    def drawBoundingBox(self):
        """
        Draw a bounding box around everything on 42 Eco1.User layer
        """
        width = 0.15
        coords = self.getBoundingBox().corners
        coords = np.append(coords, [coords[0]], axis=0)  # close the loop

        for i in range(4):
            line = sp.Sexp(
                "gr_line",
                [
                    KicadPCB.coordsToSexp("start", coords[i]),
                    KicadPCB.coordsToSexp("end", coords[i + 1]),
                    sp.Sexp("layer", "Eco1.User"),
                    sp.Sexp("width", KicadPCB.formatToString(width)),
                ],
            )
            self.gr_line = line

    def drawText(self, coords, text):
        """
        Adds a text object on Eco1.User at the given coordinates.
        """
        self.gr_text = sp.SexpParser(
            sp.parseSexp(
                """(gr_text \"{}\"
            (at {:.2f} {:.2f})
            (layer Eco1.User)
            (effects (font (size 1 1) (thickness 0.1524)))
            )
        """.format(
                    text, *coords
                )
            )
        )

    def _rename_nets(self, other_pcb):
        # find the overlapping net names
        new_nets = dict()
        old_nets = dict()

        for net in self.net[1:]:
            old_nets[net[1]] = net[0]

        for net in other_pcb.net[1:]:
            new_nets[net[1]] = net[0]

        to_rename = old_nets.keys() & new_nets.keys()
        to_renumber = set()

        renamed_nets = OrderedDict()  # name changes
        renumbered_nets = OrderedDict({0: 0})  # numbering changes
        existing_nets = OrderedDict()  # net number to name mapping
        # count the existing nets and map them
        max_net = 0

        # take inventory of the existing nets and their number
        for n in self.net:
            existing_nets[n[1]] = n[0]
            if n[0] > max_net:
                max_net = n[0]

        # rename the overlapping nets
        for net in to_rename:
            old_number = new_nets[net]
            # don't rename global nets for now
            if other_pcb.net[old_number][1][0] == '"':
                if net in existing_nets:
                    to_renumber.add(net)
                    # re-number but don't rename
                    print(
                        f"renumbering global net {net} from {old_number} to {existing_nets[net]}")
                    renumbered_nets[old_number] = existing_nets[net]
            else:
                # TODO: think of something "smarter" than this
                new_name = f"\"{net[1:-1]}-m\""
                renamed_nets[net] = new_name
                max_net += 1
                renumbered_nets[old_number] = max_net

                # append it to the list
                s = sp.SexpParser(sp.parseSexp(f"(net {max_net} {new_name})"))
                self.net._append(s)

                print(
                    f"net renamed from {net}:{old_number} to {renamed_nets[net]}:{max_net}")

        # re-number the unique nets we'll append too
        for net in new_nets:
            if net in to_rename and net not in to_renumber:
                # already re-numbered
                print(f"net {net} already re-numbered")
                continue

            old_number = new_nets[net]

            if net in to_renumber:
                renumbered_nets[old_number] = existing_nets[net]
            else:
                max_net += 1
                renumbered_nets[old_number] = max_net

                # append new, unique net
                s = sp.SexpParser(sp.parseSexp(f"(net {max_net} {net})"))
                self.net._append(s)

            print(
                f"net {net} renumbered from {old_number} to {renumbered_nets[old_number]}")

        # rename and renumber nets in modules
        for module in other_pcb.module:
            for pad in module.pad:
                name = pad.net[1]
                if name in renamed_nets:
                    name = renamed_nets[name]

                pad.net[0] = renumbered_nets[pad.net[0]]
                pad.net[1] = name

        # rename and renumber nets in zones
        for zone in other_pcb.zone:
            zone.net = renumbered_nets[zone.net]
            if zone.net_name in renamed_nets:
                zone.net_name = renamed_nets[zone.net_name]

        # renumber nets in vias
        for via in other_pcb.via:
            via.net = renumbered_nets[via.net]

    def _move_other_pcb(self, other_pcb):
        # move the pcb to be merged out of the way first
        bb_self = self.getBoundingBox()
        bb_other = other_pcb.getBoundingBox()

        # if either of the bounding boxes is not valid, there's nothing to move around
        if not (bb_self.valid and bb_other.valid):
            print("one of the bounding boxes is invalid, skipping")
            return

        x1, y1 = bb_self.center
        x2, y2 = bb_other.center
        h1 = bb_self.height
        h2 = bb_other.height
        c1 = bb_self.corners
        c2 = bb_other.corners

        # overlap check, TODO: calculate bounding boxes for all modules and distribute evenly?
        if c1[0][0] < c2[2][0] and c1[2][0] > c2[0][0] and c1[0][1] > c2[2][1] and c1[2][1] < c2[0][1]:
            # if they overlap, move the new PCB to center of the existing one + h/2 + overlap upwards
            pass
        else:
            print("modules overlap, move them out of the way")
            # TODO: make this better, we should always have a defined gap between the modules, no matter their geometry
            other_pcb.movexy(0, h2 + h1)

    def add_pcb(self, other_pcb):
        """
        Append everything from a second PCB to this one.
        No checking against overlapping components or tracks.
        Nets will be merged based on the net name, prepare them first.
        """

        # do the net renaming dance
        self._rename_nets(other_pcb)

        # move everything out of the way
        self._move_other_pcb(other_pcb)

        # Copy bulk PCB data
        total = 0
        copy_parts = [
            "module",
            "zone",
            "via",
            "segment",
            "arc",
            "gr_text",
            "gr_line",
            "gr_poly",
            "gr_arc",
            "gr_circle",
            "gr_curve",
            "dimension",
        ]
        for part in copy_parts:
            i = 0
            for thing in sp.SexpList(other_pcb[part]):
                """print(
                    " [{:05d}, {:04d}] Copying into {}...".format(total, i, part),
                    flush=True,
                )"""
                i += 1
                total += 1
                self[part] = thing

        # Not touching page size for now

        # Merge layers
        """
        We assume that layers are ordered, and they have a layer ID which is consistent
        across every PCB (this is what we see from PCBnew 5.1.6 at least)
        According to PCBnew file format docs, copper layers can be renamed.
        TODO This is not handled as of now.
        """
        other_layers = PCBlayerAbstraction(other_pcb.layers)
        # pylint: disable=access-member-before-definition
        own_layers = PCBlayerAbstraction(self.layers)
        added_layers = own_layers.merge(other_layers)
        own_layers.setAllVisible()
        if added_layers > 0:
            del self.layers
            self.layers = own_layers.getSexpObject()
        else:
            for layer in self.layers:
                if len(self.layers[layer]) > 2:
                    del self.layers[layer][2]  # make it visible

        # Merge setup

        # Merge nets

        # Merge net classes

        # Merge general
        del self.general.modules
        self.general.modules = len(self.module)
        del self.general.tracks
        self.general.tracks = len(self.via) + len(self.segment)
        # self.general.drawings = 0 # It is not clear what this value is!

    @staticmethod
    def load(filename):
        with open(filename, "r") as f:
            return KicadPCB(sp.parseSexp(f.read()))

    @staticmethod
    def empty():
        return KicadPCB(sp.parseSexp(empty_pcb.empty_pcb))
