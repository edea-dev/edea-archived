"""
Copyright (c) 2021 Fully Automated OÜ
SPDX-License-Identifier: EUPL-1.2
"""


import argparse
import os
from schematic import Module
from kicadproject import Project


def main():
    parser = argparse.ArgumentParser(
        description="Insert subcircuit(s) into a (new) KiCad project including its PCB layout.",
        epilog="Compatible with KiCad v5"
    )
    parser.add_argument(
        "--overwrite",
        help='Enable overwriting of existing project',
        action='store_true',
        dest='overwrite',
        default=False
    )
    parser.add_argument(
        "--output",
        help='Output project name',
        type=str,
        required=True
    )
    parser.add_argument(
        "--module",
        # type=argparse.FileType("d"), # this isn't a thing :/
        type=str,
        help="Directory of the module you want to insert",
        required=True,
        nargs="+"
    )
    parser.add_argument(
        "--parse",
        type=str,
        default=""
    )

    args = parser.parse_args()
    output_project = args.output
    modules = args.module

    global p
    p = Project(output_project)
    p.overwrite_enabled = args.overwrite

    # TODO make sure no eeschema is running with the target open - see psutil
    # https://thispointer.com/python-check-if-a-process-is-running-by-name-and-find-its-process-id-pid/

    # prepare output directory
    p.create_output_folder()
    if args.parse != "":
        p.schematic.parse(os.path.abspath(args.parse))
        p.schematic.findBoundingBox()

    # instantiate modules
    [x0, y0] = p.schematic.target_loc
    for module_spec in modules:
        module = Module(module_spec, [x0, y0], p.eeschema_gap)
        [x0, y0] = module.subckt.getNextX0Y0()
        p.schematic.subcircuits.append(module.subckt)
        module.copy_data(p.project_output_dir,
                         module.schematic_filename, module.schematic_filename)
        p.pcb.add_pcb(module.pcb.pcb)

    with open(p.schematic_filepath, "wt") as out_sch:
        for line in p.schematic.render():
            print(line, file=out_sch)

    p.pcb.export(p.pcb_filepath)

    return


if __name__ == '__main__':
    main()
