# EDeA Portal

## An Open Platform for Easily Reusable Subcircuits

This repository contains the Portal part of EDeA, e.g. what you (will) see on [edea.dev](https://edea.dev).

## Organization

### Backend

The [backend](backend/) is written in Go with a sprinkle of SQL and needs PostgreSQL to run.

### Frontend

The [frontend](frontend/) is written with lots of HTML, CSS and also some JavaScript to make it nicer to use on a day-to-day basis. It is currently based on Bootstrap v4.

### KiCad Project Merge Tool (EMT)

The [EDeA Merge Tool](tool/) (EMT) is a python program that contains necessary functions used by the EDeA backend. It can also be run stand-alone to merge two or more KiCad projects including the PCB layout while maintaining logical association between schematic diagram and board.

### Documentation

An important part of our work is to keep all the parts that make and tie EDeA together documented. Have a look: [doc](doc/).

## License

The portal is licensed under the [EUPL-1.2](LICENSE.md). We strongly believe in "Public money, public code" and as our initial work on EDeA is being funded by [NLnet](https://nlnet.nl/project/ElectronicsHub/) under [NGI Zero Discovery](https://nlnet.nl/discovery/), the EUPL-1.2 is the best option so improvements stay in the public and parts can simply be re-used in other projects.
