+++
title = "Introducing EDeA"
date = 2020-07-30
[taxonomies]
categories = ["Updates"]
authors = ["ln", "Zoé"]
+++

## An Open Platform for Easily Reusable Subcircuits

In software, one thing we all take for granted are libraries, enabling reusable software modules, bundling functionality together, and avoiding reimplementing the wheel over and over again.

In electronics, we have integrated circuits, so we don't have to build everything with just discrete components. This enables more complex functionality in smaller size, reduces the complexity designers need to handle, and enables people to create more.

Thanks to easily accessible development boards, like Arduino or Adafruit Feather, building prototypes got a lot easier. However, when we want to leave the wire spaghetti behind, and put every module neatly on a single PCB, we hit a wall.

Electronics hardware development is not only fragmented between multitudes of EDA tools. Reusing a design, or composing a project of multiple designs, is still next to impossible in 2020. Not even the elsewhere so ubiquitous (and sometimes carelessly done) copy-paste tricks work; having to juggle a PCB layout and a logical schematic simultaneously presents an inpenetrable barrier to most development tools.

My own journey into electronics only started two years ago. I have been using computers for ages, and was fascinated by what's below the cover. I am a self-taught electronics tinkerer, who is intimately familiar with software development tools and practices, and peeking into the world of hardware development got me amazed.

After the initial rush and fascination wears off a bit, and building electronics becomes more mundane and part of my daily life, I started to wonder: why do I have to build some parts over and over again? Where's my `go get` or `pip install` for that DC/DC converter I need? And why can't I even reuse my own schematics, and copy-paste between KiCad instances? 

At least the last part will come whenever KiCad 6 gets released (and is already available in the nightly builds). But the other issue still stands: how can we share subcircuits we built? Trying to collaborate with other people on open hardware projects showed us how impossible it is today. 

Often it takes many hours of consideration and consulting datasheets, reading application notes and dead-tree text books to get something right. Then meticulously adjusting the traces to get the best performance out of a layout, or to squeeze everything into the smallest area possible. Something I will need to do again, even when using the very same parts in the next project! Why can't I easily share the fruits of this work with others? To achieve more with less work, we need to stand on the shoulders of giants, like we do in software today. We can often just include a library, call an init function with the right parameters and get on with it. Read the documentation so we understand what we must know, but we don't always need to learn how exactly it works. So why not here too?

There is a growing Open Hardware movement, a diverse bunch of people with intimidating skill levels, and they are reimplementing the wheel over and over. What if it were easy to share those tricks, to reuse what we already made, tested, so we can build better, cheaper, or achieve more? What if we could drop a known good layout into a design? And maybe even dynamically parametrise components, so that we don't have to calculate everything by hand or by obscure spreadsheets?

This is what inspired the EDeA project. Out of a very naïve "how hard can this be?" question, we first built a primitive prototype tool to merge KiCad projects, including their schematics and PCB layout. This still need a lot of work before it can be considered safe, including correct net aliasing, nesting of subschematics, etc. But this solves only one part of the problem, something which should be solved in the upcoming major release of KiCad anyways.

We are now laying the groundwork for EDeA; a community portal to share, find, and assemble subcircuits into KiCad projects. It's all in rough shape, and we're still a bit away from the first alpha we will show to the public, but we're getting there.

What we envisioned is an easy-to-use catalog of various circuit submodules; power supplies, data converters, microcontrollers, processors, and so on. These submodules contain schematics and a PCB layout, among with useful metadata; number of copper layers, component count, surface area, necessary manufacturing capabilities, and so on. Each of the subcircuit category should also have meaningful parameters; for example efficiency for a power supply, bandwidth for a transceiver. You can select any amount of these submodules, click a button, and get a KiCad project which contains all the submodules as hierarchical subsheets. Now you only need to wire these together as you need them, and in pcbnew move the already layouted submodules to fit the exact shape you need. To keep the already complex project manageable, we can't go into auto-connecting and auto-placing of submodules. At least not yet.

So stay tuned while we finish the boilerplate and the back-end, and we will get back to you! We count on your feedback and your help on filling the repository with invaluable circuits.

## NLnet

Our work on EDeA is being funded by the [NLnet Foundation](https://nlnet.nl) ([project link](https://nlnet.nl/project/ElectronicsHub/)) so that we can work on the project full-time during the next months. This also means that significant effort will be put into it being accessible and we're committed to respecting your privacy. We're interested in open collaboration and not user metadata. EDeA will also always be completely Open Source and free for everyone to host themselves if they choose to do so.

## tl;dr

We're building an open-source web portal for sharing KiCad subcircuits, which will enable you to create more by doing less. 
