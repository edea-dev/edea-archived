#!/usr/bin/env bash

# Turn on shell verbose mode
set -v

npm version && npm install && npm audit fix

