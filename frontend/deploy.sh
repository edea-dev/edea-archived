#!/usr/bin/env bash
set -v
# This file is for internal use only

tar cfav snapshot.tar.zst public_html
scp snapshot.tar.zst automated.ee:
ssh automated.ee tar xvf snapshot.tar.zst
# https://fully.automated.ee:8081/
