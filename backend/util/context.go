package util

// SPDX-License-Identifier: EUPL-1.2

var (
	// ContextKey for user information stored for logged in users
	UserContextKey = struct{}{}
)
